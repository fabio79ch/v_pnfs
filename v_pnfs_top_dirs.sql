-- it needs the materialized view v_pnfs
DROP   MATERIALIZED VIEW IF EXISTS v_pnfs_top_dirs;
CREATE MATERIALIZED VIEW           v_pnfs_top_dirs AS

   select totMB,
          depth,
           iuid,
           igid,
         ictime,
         iatime,
           path 
   from pnfs_dirs('/pnfs')
   order by totMB desc, path ASC ;


ALTER TABLE public.v_pnfs_top_dirs OWNER TO nagios;


