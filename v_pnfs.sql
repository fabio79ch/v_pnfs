drop MATERIALIZED view v_pnfs cascade;

CREATE MATERIALIZED VIEW v_pnfs AS
    
    with 
      partial_v_pnfs  AS (
      SELECT 
           pnfs_dir.depth, 
           pnfs_dir.itype,
           pnfs_dir.iio,
           pnfs_dir.ipnfsid, 
           pnfs_dir.path, 
           pnfs_dir.iname,
           pnfs_dir.isize,
           pnfs_dir.iuid, 
           pnfs_dir.igid, 
           pnfs_dir.ictime, 
           pnfs_dir.iatime, 
           pnfs_dir.imtime,
           pnfs_dir.icrtime,
           t_level_2.imode
      FROM pnfs_dir('pnfs') pnfs_dir LEFT OUTER join t_level_2 on pnfs_dir.ipnfsid = t_level_2.ipnfsid
      ),

      t_locationinfo_aggregated AS (
      SELECT 
           t_locationinfo.ipnfsid,
           string_agg( t_locationinfo.ilocation,' ') as ilocation_aggregated
      FROM t_locationinfo
           group by t_locationinfo.ipnfsid
      ),
 
      partial_v_pnfs2 as (
      SELECT
           partial_v_pnfs.depth, 
           partial_v_pnfs.itype,
           partial_v_pnfs.iio,
           partial_v_pnfs.ipnfsid,
           partial_v_pnfs.path,
           partial_v_pnfs.iname,
           partial_v_pnfs.isize,
           partial_v_pnfs.iuid,
           partial_v_pnfs.igid,
           partial_v_pnfs.ictime,
           partial_v_pnfs.iatime,
           partial_v_pnfs.imtime,
           partial_v_pnfs.icrtime,
           partial_v_pnfs.imode,
           t_locationinfo_aggregated.ilocation_aggregated
         from partial_v_pnfs LEFT OUTER join t_locationinfo_aggregated on partial_v_pnfs.ipnfsid = t_locationinfo_aggregated.ipnfsid 
         order by partial_v_pnfs.path desc  
      )
    SELECT 
         partial_v_pnfs2.depth,
         partial_v_pnfs2.itype,
         partial_v_pnfs2.iio,
         partial_v_pnfs2.ipnfsid,
         partial_v_pnfs2.path,
         partial_v_pnfs2.iname,
         partial_v_pnfs2.isize,
         partial_v_pnfs2.iuid,
         partial_v_pnfs2.igid,
         partial_v_pnfs2.ictime,
         partial_v_pnfs2.iatime,
         partial_v_pnfs2.imtime,
         partial_v_pnfs2.icrtime,
         partial_v_pnfs2.imode,
         t_inodes_checksum.isum as adler32,
         partial_v_pnfs2.ilocation_aggregated as pools
       from partial_v_pnfs2 LEFT OUTER join t_inodes_checksum on t_inodes_checksum.ipnfsid = partial_v_pnfs2.ipnfsid  
;
         

ALTER TABLE public.v_pnfs OWNER TO nagios;

