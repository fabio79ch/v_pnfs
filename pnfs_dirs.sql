DROP FUNCTION IF EXISTS    pnfs_dirs(path  varchar) CASCADE ;

CREATE or replace FUNCTION pnfs_dirs(path  varchar) 
RETURNS TABLE(

   totMB integer,
   depth integer,
   ipnfsid varchar,
   path varchar, 
   iuid integer,
   igid integer,
   ictime  timestamp,
   iatime  timestamp,
   imtime  timestamp,
   icrtime timestamp
  
) as
$BODY$
  # PL/Python function body
  # http://www.postgresql.org/docs/9.4/static/plpython-database.html
  # The PL/Python language module automatically imports a Python module called plpy. The functions and constants in this module are available to you in the Python code as plpy.foo

  #import logging
  #logging.basicConfig(filename='/tmp/plpy.log', level=logging.INFO)
  #logging.info("START")

  thepath = path
  assert thepath.startswith('/pnfs')
  if thepath.endswith('/'): thepath = thepath[:-1]

  # dir_du_mb | depth |               ipnfsid                |             path             | iuid | igid |           ictime           |           iatime           |           imtime | icrtime 
  QUERY = """
   WITH pnfs_dirs AS (
          SELECT sum(v_pnfs.isize) AS bytes,
             t_dirs.iparent        AS ipnfsid
           FROM v_pnfs 
           LEFT JOIN t_dirs ON t_dirs.ipnfsid = v_pnfs.ipnfsid
           WHERE v_pnfs.path ~~ '"""+thepath+"""/%'
           GROUP BY t_dirs.iparent
           ORDER BY sum(v_pnfs.isize) DESC
         )
   SELECT trunc(pnfs_dirs.bytes / 1000::numeric / 1000::numeric) AS dir_du_mb,
     v_pnfs.depth,
     v_pnfs.ipnfsid,
     v_pnfs.path,
     v_pnfs.iuid,
     v_pnfs.igid,
     v_pnfs.ictime,
     v_pnfs.iatime,
     v_pnfs.imtime,
     v_pnfs.icrtime
    FROM v_pnfs
    JOIN pnfs_dirs ON v_pnfs.ipnfsid = pnfs_dirs.ipnfsid
    ORDER BY depth desc;
  """

  rv = plpy.execute( QUERY )

  #logging.info("QUERY EXECUTED")

  from ordereddict import OrderedDict
  D = OrderedDict()
 

  for x in rv             : 
      #logging.info( x )
      D[x['path']] = {
                      'myMB'            : int(x['dir_du_mb'])  ,
                      'depth'           : int(x['depth'])      , 
                      'ipnfsid'         :     x['ipnfsid']     ,
                      'path'            :     x['path']        ,
                      'iuid'            : int(x['iuid'])       ,
                      'igid'            : int(x['igid'])       ,
                      'ictime'          :     x['ictime']      ,
                      'iatime'          :     x['iatime']      ,
                      'imtime'          :     x['imtime']      ,
                      'icrtime'         :     x['icrtime']     ,
                      'totMB'           : int(x['dir_du_mb'])   } 
  import re
  # this works because in the QUERY we used "ORDER BY depth desc;", so each totMB(son(x)) is computed before of totMB(x)  
  #         !! AND !!
  # we used OrderedDict() that respects that order as well !
  for x in D.keys(): # for each /pnfs path
    xFather = re.sub( '/[^\/]*$', '', x )  # the father of that path
    if  xFather != re.sub( '/[^\/]*$', '', thepath ) and xFather != '/pnfs' :                
       #D[ the father of that path ]['totMB'] <= that path contributes with its totMB 
        D[ xFather                 ]['totMB'] += D[x]['totMB'] 

  L = [ ( D[x]['totMB'],  D[x]['depth'], D[x]['ipnfsid'], x, D[x]['iuid'],D[x]['igid'],D[x]['ictime'],D[x]['iatime'],D[x]['imtime'],D[x]['icrtime'] ) for x in D ]
 
  #logging.info("CREATING OUTPUT LIST")
  #for x in L: logging.info( x )

  return  L

$BODY$ 
LANGUAGE 'plpythonu';

ALTER FUNCTION public.pnfs_dirs(character varying) OWNER TO nagios;
