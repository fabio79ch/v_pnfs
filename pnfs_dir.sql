DROP FUNCTION IF EXISTS pnfs_dir(character varying) CASCADE ;

CREATE OR REPLACE FUNCTION pnfs_dir(character varying) RETURNS TABLE(
                                                          depth integer, 
                                                          itype integer,
                                                            iio integer,
                                              ipnfsid character varying, 
                                                              path text, 
                                                iname character varying, 
                                                           isize bigint, 
                                                           iuid integer, 
                                                           igid integer, 
                                        ictime timestamp with time zone, 
                                        iatime timestamp with time zone, 
                                        imtime timestamp with time zone,
                                       icrtime timestamp with time zone
                                                                       )
    LANGUAGE plpgsql STABLE
    AS $_$
DECLARE
  ID character varying;
BEGIN
  SELECT path2inode('000000000000000000000000000000000000', $1 ) INTO ID ;

  return query

  with recursive spacedir(mydepth, myitype, myiio, myipnfsid, mypath, myiname, myisize, myiuid, myigid, myictime, myiatime, myimtime, myicrtime  ) as (
   (select         1, I.itype, I.iio, I.ipnfsid, '/'||$1||'/'||D.iname ,D.iname, I.isize, I.iuid, I.igid, I.ictime, I.iatime, I.imtime, I.icrtime
        from t_inodes I join t_dirs D on I.ipnfsid = D.ipnfsid
           where I.ipnfsid in ( select DD.ipnfsid from t_dirs DD where DD.iparent = ID and DD.iname not in ('.','..'))
              and D.iname not in ('.','..') )
    UNION
   (select mydepth+1, I.itype, I.iio, I.ipnfsid, mypath||'/'||D.iname            ,D.iname, I.isize, I.iuid, I.igid , I.ictime, I.iatime, I.imtime, I.icrtime
         from t_inodes I join t_dirs D on I.ipnfsid = D.ipnfsid, spacedir S
            where D.iparent = S.myipnfsid
              and D.iname not in ('.','..'))   )

  select * from spacedir;

END;
$_$;


ALTER FUNCTION public.pnfs_dir(character varying) OWNER TO nagios;

-- itype
-- 16384 : directory
-- 32768 : file
-- 40960 : link

-- iio
-- 0 : data stored on pool
-- 1 : data stored in db (e.g. config files) or links

